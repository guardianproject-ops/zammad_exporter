### CLI
```
usage: zammad_exporter.py [-h] [--zammad-host ZAMMAD_HOST]
                          [--bind-address BIND_ADDRESS]
                          [--bind-port BIND_PORT] [-v]
                          [--token TOKEN | --token-file TOKEN_FILE]

Zammad exporter for Prometheus

optional arguments:
  -h, --help            show this help message and exit
  --zammad-host ZAMMAD_HOST, -z ZAMMAD_HOST
                        The zammad host, PROTO:HOST:PORT. (default
                        http://127.0.0.1:80)
  --bind-address BIND_ADDRESS, -b BIND_ADDRESS
                        The address to which the exporter should bind
                        (default: 0.0.0.0)
  --bind-port BIND_PORT, -p BIND_PORT
                        Port the exporter should listen on. (default 9390)
  -v, --version         Display version information and exit
  --token TOKEN         Zammad monitoring token.
  --token-file TOKEN_FILE
                        File containing the Zammad monitoring token
```
