#!/usr/bin/env python3
from datetime import datetime

import pytest
import requests

from zammad_exporter import (
    ZammadCollector,
    get_token,
    date_to_seconds,
    check_access,
    cli,
)


def test_date_to_seconds():
    now = datetime.strptime("2019-01-24T18:11:30.163413Z", "%Y-%m-%dT%H:%M:%S.%fZ")
    seconds = date_to_seconds("2019-01-22T11:40:03.901Z", now=now)
    assert seconds == 196286


def test_get_token():
    token = get_token("foo", None)
    assert token == "foo"


health_url = "http://127.0.0.1/api/v1/monitoring/health_check?token=foo"
status_url = "http://127.0.0.1/api/v1/monitoring/status?token=foo"


def test_check_access_500(requests_mock):
    requests_mock.get(health_url, status_code=500)
    assert not check_access("http://127.0.0.1", "foo")


def test_check_access_timeout(requests_mock):
    requests_mock.get(health_url, exc=requests.exceptions.ConnectTimeout)
    assert not check_access("http://127.0.0.1", "foo")


def test_check_access_200(requests_mock):
    requests_mock.get(health_url, status_code=200)
    assert check_access("http://127.0.0.1", "foo")


def test_exporter_invalid_json(requests_mock):
    requests_mock.get(health_url, status_code=200, text="invalid json")
    collector = ZammadCollector("http://127.0.0.1", "foo")
    names = ["zammad_healthy"]
    values = [False]
    for m, i in enumerate(collector.collect()):
        assert i.name == names[0]
        assert i.samples[0].value == values[0]


def test_exporter_non_200(requests_mock):
    requests_mock.get(health_url, status_code=500, text="{}")
    requests_mock.get(status_url, status_code=500, text="{}")
    collector = ZammadCollector("http://127.0.0.1", "foo")
    names = ["zammad_healthy"]
    values = [False]
    for i, m in enumerate(collector.collect()):
        assert m.name == names[i]
        assert m.samples[i].value == values[i]


@pytest.fixture()
def health_happy():
    with open("fixtures/health_happy.json", "r") as f:
        return f.read()


@pytest.fixture()
def status_happy():
    with open("fixtures/status_happy.json", "r") as f:
        return f.read()


happy_metrics = [
    "zammad_agent_count",
    "zammad_user_count",
    "zammad_group_count",
    "zammad_overview_count",
    "zammad_ticket_count",
    "zammad_ticket_article_count",
    "zammad_text_module_count",
    "zammad_storage_bytes",
    "zammad_last_login_seconds",
    "zammad_last_created_user_seconds",
    "zammad_last_created_group_seconds",
    "zammad_last_created_overview_seconds",
    "zammad_last_created_ticket_seconds",
    "zammad_last_created_ticket_article_seconds",
    "zammad_last_created_text_module_seconds",
    "zammad_healthy",
]


def test_exporter_happy(requests_mock, health_happy, status_happy):
    requests_mock.get(health_url, status_code=200, text=health_happy)
    requests_mock.get(status_url, status_code=200, text=status_happy)
    collector = ZammadCollector("http://127.0.0.1", "foo")
    for i, m in enumerate(collector.collect()):
        assert m.name == happy_metrics[i]
        print(m.name)


def test_cli():
    parser = cli()
    args = parser.parse_args([])
    assert args.bind_address == "0.0.0.0"
    assert args.bind_port == 9390
    assert args.zammad_host == "http://127.0.0.1:80"
