requests-mock==1.7.0
pytest
black
jedi
flake8
-r requirements.prod.txt
